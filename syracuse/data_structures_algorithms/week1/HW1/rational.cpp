#include <iostream>
#include "rational.hpp"

// Default (zero-argument) constructor
Rational::Rational()
{
    numer = denom = 0;
}

// Two argument constructor
Rational::Rational(int num, int den)
{
    numer = num;
    denom = den;

    simplify();
}

void Rational::setNumerator(int num)
{
    numer = num;
}

void Rational::setDenominator(int den)
{
    denom = den;
}

int Rational::getDenominator()
{
    return denom;
}

int Rational::getNumerator()
{
    return numer;
}

void Rational::print()
{
    if( denom == 0)
    {
        std::cout << "\nError: Cannot Divide By Zero" << std::endl; 
    }
    else if( numer == 0)
    {
        std::cout << 0;
    }
    else{
        std::cout << numer << '/' << denom << std::endl;
    }
}

// returns a + b
Rational Rational::add(Rational rhs)
{
    Rational result;

    int num = numer * rhs.denom + rhs.numer * denom;
    int den = denom * rhs.denom;

    result.numer = num;
    result.denom = den;

    // Simplify the result if possible.  Note that we 
    // are not passing "result" as an argument to
    // "simplify".  Instead we are calling the "simplify"
    // function of the "result" object.

    result.simplify();

    return result;
}

// Returns a * b
Rational Rational::multiply(Rational rhs)
{
    Rational result;

    result.numer = numer * rhs.numer;
    result.denom = denom * rhs.denom;

    result.simplify();

    return result;
}

void Rational::simplify()
{
    // the method of simplifying rational numbers is finding
    // the least common divisors of numerator and denominator
    // and divide both numerator and denominator by that least common divisor
    // and simplify till their least common factor is 1.
    int divisor = gcd(numer, denom);
    numer = numer / divisor;
    denom = denom / divisor;
}

// Method to find the greatest common divisor of two integers
int Rational::gcd(int a, int b)
{
    int c;

    if(a<0)
        a = a * -1;
    
    if(b<0)
        b = b * -1;
    
    while(a != 0){
        c = a;
        a = b % a;
        b = c;
    }

    return b;
}