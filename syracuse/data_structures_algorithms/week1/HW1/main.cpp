#include "rational.hpp"

int main(){

    Rational a(1, 2);
    Rational b(3, 4);
    Rational c;

    c = a.add(b);
    c.print();

    c = a.multiply(b);
    c.print();

    return 0;
}