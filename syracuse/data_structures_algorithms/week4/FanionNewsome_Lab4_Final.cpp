// Example program
#include <iostream>
#include <string>

using namespace std;


class Node{
    public:
        Node(int _data):data(_data){ next = NULL;}
        ~Node(){}
        
        int data;
        Node *next;
};

void remove(Node *& p, int v){
    if( p == NULL)
        return;
        
    if(p->data != v){
        remove(p->next, v);
    }
    else{
        Node *tmp;
        tmp = p;
        p = p->next;
        remove(p, v);
        delete tmp;
    }
}

void removeAllDuplicates(Node *& h){
 
 	// if list is empty, simply return
    if( h == NULL){
    	return;
    }
    
    // recursively call removing any duplicate 
    // item with the current node's data
    //
    remove(h->next, h->data);
    
    // keep going and call remove on the next item
    // in the list to see if there are duplicates
    // for that element as well
    //
    removeAllDuplicates(h->next);
}

void printList(Node *list){
    Node *tmp = list;
    
    if(tmp == NULL){
        std::cout << "The list is empty" << std::endl;
        return;
    }
    while(tmp != NULL){
        std::cout << tmp->data << " ";
        tmp = tmp->next;
    }
    std::cout << std::endl;
}

int main()
{
    Node *head = NULL;
    Node *n = NULL;
    int num = 0;
    
    std::cout << "Enter numbers (-1 to exit) : " << std::endl;
    std::cin >> num;
    
    while(num != -1){
        n  = new Node(num);
        n->next = head;
        head = n;
        cin >> num;
    }
    printList(head);
    std::cout << "What do you want to remove ?";
    std::cin >> num;

    remove(head, num);
    removeAllDuplicates(head);
    printList(head);
}
