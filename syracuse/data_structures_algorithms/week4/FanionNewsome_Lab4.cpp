
void remove(Node *& p, int v){
    if( p == NULL)
        return;
        
    if(p->data != v){
        remove(p->next, v);
    }
    else{
        Node *tmp;
        tmp = p;
        p = p->next;
        remove(p, v);
        delete tmp;
    }
}

void removeAllDuplicates(Node *& h){
 
 	// if list is empty, simply return
    if( h == NULL){
    	return;
    }
    
    // recursively call removing any duplicate 
    // item with the current node's data
    //
    remove(h->next, h->data);
    
    // keep going and call remove on the next item
    // in the list to see if there are duplicates
    // for that element as well
    //
    removeAllDuplicates(h->next);
}