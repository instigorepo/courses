#include <iostream>
#include <cstdlib>

using namespace std;

class TreeNode{
	public:
		int data;
		TreeNode *left;
		TreeNode *right;

		TreeNode(int element, TreeNode *lt, TreeNode *rt){
		  this->data = element;
		  this->left = lt;
		  this->right = rt;
		}
};

void displayLeaves(TreeNode *r){

	// if the node is empty, return
	if( r == nullptr){
		return;
	}

	// case for a leaf node, no left and no right child
	if(r->left == nullptr && r->right == nullptr){
		// found one so let's print it
		std::cout << r->data << " ";
		return;
	}

	// case for if there is a left child of the present node
	// so we need to recurse left
	if(r->left != nullptr){
		displayLeaves(r->left);
	}

	// case for if there is a right child of the present node
	// so we need to recurse right
	if(r->right != nullptr){
		displayLeaves(r->right);
	}
}

void inorder(TreeNode * t){

	if( t != nullptr){
		inorder(t->left);
		std::cout << t->data << " ";
		inorder(t->right);
	}
}

void inorderSort(TreeNode *t, int*arr, int &index){
	if( t != nullptr){
		inorderSort(t->left, arr, index);
		arr[index++] = t->data;
		inorderSort(t->right, arr, index);
	}
}

void insert(int x, TreeNode *& t){
	// case 1: no elements in tree
	if(t == nullptr){
	  t = new TreeNode(x, nullptr, nullptr);
	  return;
	}
	else if(x < t->data){
	  insert(x, t->left);
	}
	else if( x > t->data){
	  insert(x, t->right);
	}
	else{
		return; // item exists already
	}
}


int height(TreeNode *root){
  if(root == nullptr){
	return -1;
  }
  int leftHeight = height(root->left);
  int rightHeight = height(root->right);

  return 1 + max(leftHeight, rightHeight);

}

TreeNode* treeSort(int arr[], int n){
  // update the array so that the array elements
  // are in increasing order.  Return a pointer to the root
  //
  // insert the array's elements into a BST and place
  // the items into array

  TreeNode *sortedTree = NULL;

  if(n == 0){
	std::cout << "There are no items to sort\n";
	return NULL;
  }

  // insert the items in the array into the BST
  for(int i = 0; i < n; ++i){
	insert(arr[i], sortedTree);
  }

  int startIndex = 0;

  // create
  inorderSort(sortedTree, arr,startIndex);

  return sortedTree;

}

int main(){

  int N, num;
  TreeNode *root;

  root = NULL;

  // init random seed using current time
  srand(time(NULL));

  //cout << "Enter the size of the array: ";
  //cin >> N;
  N = 10;
  int myArray[N] = {35,61,2,74,86,92,27,87,17,18};

  // generate N random numbers within the range 1 to 100
  /*for(int i = 0; i < N; i++){
	  num = rand() % 10 + 1;
	  cout << "Inserting " << num << endl;
	  myArray[i] = num;
  }*/

  cout << "Unsorted Array: " << endl;
  for(int i = 0; i < N; i++){
	  cout << myArray[i] << " ";
  }
  cout << endl;



  root = treeSort(myArray, N);
  cout << endl;

  cout << "Sorted Array: " << endl;

  for(int i = 0; i < N; i++){
	  cout << myArray[i] << " ";
  }
  cout << endl;

  cout << "\nTree in inorder: " << endl;
  inorder(root);
  cout << endl;

  cout << "\nThe height of the tree is: " << height(root) << endl;
  cout << "The leaves of the tree are: ";
  displayLeaves(root);
  cout << endl;

  return 0;
}
