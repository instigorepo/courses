#include <iostream>

using namespace std;

int * double_array(int *arr, int n){
    
    int *newArray = NULL;
    int newSize = (2*n)+1;
    
    newArray = new int[newSize];
    
    if(newArray != NULL){
        // initialize items in new array to zero
        for(int i = 0; i < newSize; i++){
            newArray[i] = 0;
        }
    }
    else{
        std::cout << "Not enough memory!" << std::endl;
        return NULL;
    }

    // copy the items into the new array
    //
    for(int i = 0; i < n; ++i){
        newArray[i] = arr[i];
    }

    // memory clean up
    //
    if( arr != NULL){
        delete [] arr;
        arr = NULL;
    }

    return newArray;
}


int main(){
    int *arr = new int[5]; 
    arr[0] = 1;

    int size = 5;
    int *copy = double_array(arr, size);
    
    int newSize = 2*size + 1;
    for(int i = 0; i < newSize; i++){
        std::cout << copy[i] << " ";
    }
    std::cout << std::endl;    
    
    // clean up copy
    delete [] copy;
    copy = NULL;
    
    return 0;
}

