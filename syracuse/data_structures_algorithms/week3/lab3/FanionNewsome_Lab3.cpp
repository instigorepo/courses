

int * double_array(int *arr, int n){
    
    int *newArray = nullptr;
    int newSize = (2*n)+1;
    
    newArray = new int[newSize];
    
    if(newArray != nullptr){
        // initialize items in new array to zero
        for(int i = 0; i < newSize; i++){
            newArray[i] = 0;
        }
    }
    else{
        std::cout << "Not enough memory!" << std::endl;
        return nullptr;
    }

    // copy the items into the new array
    //
    for(int i = 0; i < n; ++i){
        newArray[i] = arr[i];
    }

    // memory clean up
    //
    if( arr != nullptr){
        delete [] arr;
        arr = nullptr;
    }

    return newArray;
}

void addNodeToTail(List *list, int x){
    
    // if no list instance, quit
    //
    if(list == nullptr){
        std::cout << "The list is NULL" << std::endl;
        return;
    }

    // create a new node
    //
    Node *newNode = new Node();
    
    // check if new node is allocated
    // and initialize the element
    //
    if(newNode){
        newNode->data = x;
        newNode->next = nullptr;
    }        
    
    // point to the head of the list (if there is one)
    //
    Node *tmp = list->head;
    
    // check if case of an empty list 
    //
    if(tmp == nullptr){
        list->head = newNode; 
        return;
    }
    
    // assume at least one item in the list
    // iterate to the end of the list 
    //
    while(tmp->next != nullptr){
        tmp = tmp->next;
    }
    
    // add the new node to the tail
    tmp->next = newNode;
}

List* merge ( List *a, List *b){

    // if no items in a, return list b and vice versa
    // also check for if we have a list but the head item is NULL
    //
    if((a == nullptr || b == nullptr) || (a->head == nullptr || b->head == nullptr)){ 

        return (a == nullptr || a->head == nullptr) ? b : a;
    }
    
    // create the merged list that we will return
    //
    List *merged = new List();
    Node *head = new Node();

    if(a->head->data <= b->head->data){
        head = a->head;
        a->head = a->head->next;
    }
    else{
        head = b->head;
        b->head = b->head->next;
    }

    Node *tmp = head;
    merged->head = tmp;
    
    while(a->head != nullptr && b->head != nullptr){
        if(a->head->data < b->head->data){
            tmp->next = a->head;
            a->head = a->head->next;
        }
        else{
            tmp->next = b->head;
            b->head = b->head->next;
        }
        tmp = tmp->next;
    }
    
    // handle the stragglers once we 
    // have hit the end of the one of the lists
    // so we just attach the remaining items to our
    // merged list
    if(a->head == nullptr){
        tmp->next = b->head;
    }
    else{
        tmp->next = a->head;
    }

    return merged;
}