#include <iostream>
using namespace std;

typedef struct Node {
    Node(int item){ data = item; next = NULL;}
    struct Node *next;
    int data;
} Node;

Node* mergeListsByShuffle(Node *head1, Node *head2)
{
  Node *ptr1 = head1, *ptr2 = head2, *tmp;
  bool merged = false;
  
  while (!merged)
  {
    if (ptr1 == NULL)
    {
      merged = true;
    }
    else if (ptr2 == NULL)
    {
      merged = true;
    }
    else
    {
      tmp = ptr1->next;
      ptr1->next = ptr2;
      ptr1 = tmp;

      tmp = ptr2->next;
      if (ptr1) ptr2->next = ptr1;
      ptr2 = tmp;
    }
  }
  return head1;
}

void printList(Node *head)
{
    while (head)
    {
      cout << head->data << endl;
      head = head->next;
    }
}

void rotateLeftByOne(int *array, int n)
{
  int tmp = array[0];
  for (int i = 1; i < n; ++i)
    array[i - 1] = array[i];
  array[n - 1] = tmp;
}

void rotate(int *array, int d, int n)
{
  // Rotate by one
  d %= n; // You don't need this..
  for (int i = 0; i < d; ++i)
    rotateLeftByOne(array, n);
}

int main()
{

  cout << "Insert numbers for list1, and end with -1" << endl;
  int n = 0;
  Node *head1 = NULL, *head2 = NULL;
  while (n != -1)
  {
    cin >> n;
    if (n == -1) break;
    Node *t = new Node(n);
    t->next = head1;
    head1 = t;
  }

  cout << "Insert numbers for list2, and end with -1" << endl;
  n = 0;
  while (n != -1)
  {
    cin >> n;
    if (n == -1) break;
    Node *t = new Node(n);
    t->next = head2;
    head2 = t;
  }

  cout << "Printing list1" << endl;
  printList(head1);

  cout << "Printing list2" << endl;
  printList(head2);

  Node *p = mergeListsByShuffle(head1, head2);
  cout << "Merged list: " << endl;
  printList(p);

  return 0;
}
