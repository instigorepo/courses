#include <iostream>

using namespace std;

class Node{
    public:
        int data;
        Node *next;
};

Node *back;

void enqueue(int item){
  Node *nd = new Node();

  if(nd != nullptr){
    nd->data = item;
    nd->next = NULL;
  }
  nd->next = back->next;
  back->next = nd;
  back = nd;
}

Node* dequeue(){
  Node *tmp = NULL;

  if(back->next == back){
    tmp = back;
    back = NULL;
  }
  else{
    tmp = back->next;
    back = tmp->next;
  }
  return tmp;
}

int main(){

  return 0;
}
