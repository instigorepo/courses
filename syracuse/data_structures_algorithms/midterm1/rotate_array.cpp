#include <iostream>

using namespace std;

void rotate(int arr[], int d, int n){
  int tmp = 0, j = 0;
  for(int i = 0; i < d; i++){
    tmp = arr[0];
    for(j = 0; j < n -1; j++){
      arr[j] = arr[j+1];
    }
    arr[j] = tmp;
  }

  for(int i = 0; i < n; ++i){
    cout << arr[i] << " ";
  }
  cout << endl;
}

int main(){
  int arr[] = {1,2,3,4,5,6,7};

  rotate(arr, 2, 7);

  return 0;
}
