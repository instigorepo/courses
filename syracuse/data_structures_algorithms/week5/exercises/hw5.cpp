#include <iostream>

using namespace std;

struct TreeNode{
  TreeNode(int item){ data = item; left = NULL; right = NULL;}
  struct TreeNode *left, *right;
  int data;
};

TreeNode *root = NULL;

void inorder(TreeNode *root){

  if(root != nullptr){
    if(root->left) inorder(root->left);
    std::cout << root->data << " ";
    if(root->right) inorder(root->right);
  }
}

void insert(TreeNode **root, int item){

  TreeNode *tmp = *root;
  TreeNode *newNode = new TreeNode(item);

  while(tmp != NULL){
    if(tmp->data > item){
      tmp = tmp->left;
    }
    else if(tmp->data < item){
      tmp = tmp->right;
    }
    else{
      return; // same item
    }
  }
  tmp = newNode;
}

int main(){
  insert(&root,1);
  inorder(root);
  return 0;
}
