#include <iostream>

using namespace std;

struct TreeNode {
  int data;
  struct TreeNode *left;
  struct TreeNode *right;
};


int FindMax(TreeNode *p){

  int root_val, left, right, max;
  max = -1; // assume all values are positive

  if(p != NULL){
    root_val = p->data;
    left = FindMax(p->left);
    right = FindMax(p->right);

    // find the largest of the three
    if( left > right){
      max = left;
    }
    else{
      max = right;
    }

    if(root_val > max){
      max = root_val;
    }
  }
  return max;
}


int main(){


  return 0;
}
